<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class DefaultController extends AbstractController
{
    #[Route('/', name: 'accueil')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'page' => "accueil"
        ]);
    }

    #[Route('/page/{page}', name: 'view_page')]
        public function viewPage($page)
        {
            return $this->render('Pages/' . $page . '.html.twig', [
                'controller_name' => 'DefaultController',
    	        'page' => $page
            ]);
        }

        #[Route('/contact', name: 'contact')]
        public function viewPageContact()
        {
            return $this->render('Pages/contact.html.twig', [
                'controller_name' => 'DefaultController',
    	        'page' => 'contact'
            ]);
        }

        #[Route('/page-privacy/{page}', name: 'view_page_privacy')]
        public function viewPagePrivacy($page)
        {
            return $this->render('Pages/privacy/' . $page . '.html.twig', [
                'controller_name' => 'DefaultController',
                'page' => $page
            ]);
        }

}
