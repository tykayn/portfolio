/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
// require('./bootstrap');

document.addEventListener('DOMContentLoaded', function() {
    const toggleButton = document.getElementById('lights-toggle');
    const labelText = document.querySelector('.label-text');

    toggleButton.addEventListener('click', function() {
        document.body.classList.toggle('dark-mode');
        if (document.body.classList.contains('dark-mode')) {
            labelText.innerHTML = 'Sombre';
        } else {
            labelText.innerHTML = 'Clair';
        }
    });
});

