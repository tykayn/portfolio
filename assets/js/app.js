// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');
console.log('body is waiting ...');
$(document).ready(function () {
    console.log('body is ready');
    $('[data-toggle="popover"]').popover();
    $('body').addClass('is-loaded');

    console.log('accueil script assets');
    console.log('$', $, jQuery);
    var limit_display_articles = 2;
    var cipherHtml = '';
    var tkHtml = '';
    $.get('https://www.cipherbliss.com/wp-json/wp/v2/posts').then(function (rep) {
        var articles = rep;
        console.log('articles loaded from cipherbliss', articles);
        for (let i = 0; i < limit_display_articles; i++) {
            cipherHtml += `<li>
<a href="${articles[i].link}">
    <article class="media">
        <div class="media-content">
            <div class="content">
              <p>
                <strong>
                  ${articles[i].title.rendered}
                </strong> 
               </p>
            </div>
        </div>
    </article>
</a>
</li>`;
        }
        $('#latest-cipherbliss').html('<ul>' + cipherHtml + '</ul>');

    });
    $.get('https://www.tykayn.fr/wp-json/wp/v2/posts').then(function (rep) {
        var articles = rep;
        console.log('articles from tykayn.fr', articles);
        for (let i = 0; i < limit_display_articles; i++) {
            tkHtml += `<li>
<a href="${articles[i].link}">
    <article class="media">
        <div class="media-content">body is ready
            <div class="content">
              <p>
                <strong>
                  ${articles[i].title.rendered}
                </strong>
               </p>
            </div>
        </div>
    </article>
</a>
</li>`;
        }
        $('#latest-tykayn').html('<ul>' + tkHtml + '</ul>');

    });

    $("#lights-toggle").on('click', function(){
        console.log('change light');
        $('body').toggleClass('lights-off');
        if($('.label-text').html() == "sombre"){
            $('.label-text').html("clair");
        }else{

            $('.label-text').html("sombre");
        }
    })
});
// require('./switch.min');
// require('./24hbd');
