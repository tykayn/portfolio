jQuery(document).ready(function () {

    /**
     * Created by tykayn on 27/03/15.
     */
// constantes
    msec = 60 * 60 * 24 * 1000; // milisecondes dans une journée
    secjour = 24 * 60 * 60 * 1000; // nombre de secondes dans une journée
    if (new Date().getMonth() < 2) {
        eventBD = 24;
    }
    else {
        eventBD = 23;
    }

    logToHours = function (microseconds) {
        //console.log('log ' + (microseconds / 1000 / 60 / 60) + " heures")
        //console.log('log ' + (microseconds / 1000 / 60 / 60 / 24) + " jours")
    }
    /* mettre les valeurs par défaut */
    getVals = function () {
        console.log("getvals ");
        start = $('#start');
        end = $('#end');
        jours = $('#jours');
        heures = $('#heures');
        pages = $('#pages');
        ppj = pages.val() / (jours.val() * 1 + 24 / heures.val()*1);
        pages_finies = $('#pages_finies');
        $('#reste').html(reste);
        $('#ppj').html(ppj.toFixed(1));
        nbpages = pages.val()*1
        if(!nbpages){
            nbpages = 1;
        }
        nbpages_faites = pages_finies.val()*1
        if(!nbpages_faites){
            nbpages_faites = 0;
        }
        //pourcent_temps=0;
        pourcent = nbpages_faites / nbpages * 100;
        reste = nbpages - nbpages_faites;
        diff = new Date(end.val()) - new Date(start.val());
        diff_now = new Date() - new Date(start.val());
        if(diff>0 && diff_now>0){
            pourcent_temps = diff_now / diff * 100;
        }
        else {
            pourcent_temps = 0
        }
        console.log("diffs " , diff_now, diff)

    }
    /**
     * convertir un objet date en chaine date et heure lisible
     * @returns {String|rendu}
     */
    date2String = function (d) {
        return d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2) + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    }
    date2StringNoHours = function (d) {
        return d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2);
    }
    date2StringJustHours = function (d) {
        return d.getHours() + 'h ' + d.getMinutes() + ':' + d.getSeconds();
    }
    date2StringBegin = function (d) {
        return d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2);
    }

    // comparer si la date est aujourdhui
    compareTodayStrings = function(d){
        var rep;
        var today = new Date();
        if(date2StringNoHours(d) == date2StringNoHours(today)){
            rep = 'today';
        }
        else if(d.getTime() - today.getTime() < 1000*60*60*24){
            rep = 'tomorrow';
        }
        return rep;
    }
    /**
     * feuille de route: couper en parties la durée du projet
     * pour savoir à quelle jour/heure doivent être finies les parties
     */
    makeRoute = function () {

        console.log('makeRoute');
        rendu = '';
        etapes = new Array();
        console.log("makeRoute diffs " , diff_now, diff)
        getVals();
        var nbpages =  pages.val() *1;
        if(!nbpages){
            nbpages = 1;
        }
        msec_etape = Math.round(diff / nbpages) // durée d'une étape en milisecondes
        for (var i = 0; i < reste; i++) {

            ajout = (msec_etape) * i;
            var d = new Date(new Date(start.val()).getTime() *1 + ajout *1);
            var new_d = date2String(d);
            var comparaison = compareTodayStrings(d);
            if( comparaison == 'today'){
                rendu += '<br/> page ' + (i + 1 + pages_finies.val() * 1 ) + ' , à finir à ' + date2StringJustHours(d);
            }
            else if( comparaison == 'tomorrow'){
                rendu += '<br/> page ' + (i + 1 + pages_finies.val() * 1 ) + ' , à finir demain à ' + new_d;
            }
            else{
                rendu += '<br/> page ' + (i + 1 + pages_finies.val() * 1 ) + ' , à finir le ' + new_d;
            }


            etapes[i] = new_d;
        }
        $('#route').html(rendu);

        /**
         * calcul des heures de la durée du projet
         */
        var h = jours.val() * 24 + heures.val() | eventBD;
        pph = pages.val() / h;
        $('#pph').html(pph.toFixed(3));
        updateProgression();
        return rendu;
    };

    reset = function () {

        localStorage.clear('24hbdTK');
        init();
    }
    /**
     * sauvegarder les données du formulaire dans le local storage si possible
     * @returns {undefined}
     */
    save = function () {
        var disquette = {
            start: start.val(),
            end: end.val(),
            jours: jours.val() * 1,
            pages: pages.val(),
            pages_finies: pages_finies.val()
        }
        var disquette = JSON.stringify(disquette);
        localStorage.setItem('24hbdTK', disquette);
        $('#confirmSaved').remove();

        $('#24hbd').prepend(' <div class="column is-6 pull-left alert alert-success alert-dismiss" id="confirmSaved" > Sauvegardé ! <br/></div>');
        $('.alert-dismiss').alert().fadeOut();
    }
    /**
     * prendre les données du formulaire dans le local storage
     * et les placer dans la page
     */
    getSave = function () {
        var new_disquette = localStorage.getItem('24hbdTK');
        new_disquette = JSON.parse(new_disquette);
        console.log(new_disquette);

        if (new_disquette !== null && new_disquette.start !== null) {
            $('#start').val(new_disquette.start);
            $('#end').val(new_disquette.end);
            $('#jours').val(new_disquette.jours);
            $('#pages').val(new_disquette.pages);
            $('#pages_finies').val(new_disquette.pages_finies);

        }

    }
    /**
     * initialiser les données de formulaire
     */
    init = function () {
        console.warn(eventBD);
        if (typeof localStorage != 'undefined') {
            getSave();
        }
        else {
            $('#launch').hide();
            $('#jours').val(1);
        }

        getVals();
        heures.val(eventBD);
        dateinit = date2StringNoHours(new Date()) + ' 12:00:00'; //le jour des 24 h de bd, mettre la bonne date de base.
        dateinit = new Date(dateinit);
        updateStart()
        updateEnd()
        console.info("jours totaux", jours.val() *1 + (heures.val() / 24));
    };

    /**
     * mettre à jour le formulaire
     */
    updateAll = function () {
        getVals();
        d = new Date(start.val());
        other_d = new Date(new Date(d).getTime() + (24 * 60 * 60 * 1000) * jours.val() * 1);
        start.val(date2String(d));
        end.val(date2String(other_d));
        makeRoute();
        updateProgression();
    };

    /**
     * barre de progression des pages
     * @returns {undefined}
     */
    updateProgression = function () {
        console.log('updateProgression')
        getVals();
        $('#avancement').val(pourcent );
        $('#reste').val(reste );
        $('#duree_barre').val(pourcent_temps );
        comment();

    };
    /**
     * la modification des jours entraine la modif de la fin de durée
     */
    updateJours = function () {

        console.log('updateJours');
        updateEnd();
    }


    /**
     * commenter l'avancement
     */
    comment = function () {
        a = $('#avancement').val();
        b = $('#duree_barre').val();
        console.log('commentaire ' + a + ' ' + b);
        $('#retard , #ok , #finished , #outTime, #loose ').hide()
        // blocs à montrer :  retard, ok , finished, outTime
        if(b==100 && a <b){
            $('#loose').show()
            console.log('finished');
        }else{
            if (a > 99) {
                $('#finished').show()
                console.log('finished');
            }
            if (new Date() > end || (a==b && b==0) ) {
                $('#outTime').show()
                console.log('outTime');
            }
            else if (a > b) {
                $('#ok').show()
                console.log('ok');
            }
            else if (b > a) {
                $('#retard').show()
                console.log('retard');
            }
        }

    };

    /**
     * le début a changé, changer la fin selon le nombre de jours de la nouvelle durée
     */
    updateStart = function () {
        console.log("updateStart")
        console.log("start + jours et heures ")


        getVals();
        var new_days_sec = (jours.val() * 1 + heures.val() / 24 ) * msec;
        var startTime = new Date(start.val()).getTime();
        //console.log("startTime " + startTime)
        //logToHours(startTime)
        console.log("new_days_sec " + new_days_sec)
        logToHours(new_days_sec)
        var valEnd = date2String(new Date(startTime + new_days_sec));
        end.val(valEnd);
        makeRoute();
    };


    /**
     * calculer la date de fin
     */
    updateEnd = function () {
        console.log("updateEnd")
        getVals();
        if (!parseInt($('#heures').val())) {
            var h =
                eventBD |
                23;
        }
        else {
            var h = parseInt($('#heures').val());
        }
        //if (h >= 24) {
        //    ajout_jours = Math.round(h / 24)
        //    jours.val(jours.val() * 1 + ajout_jours);
        //    h = h - ajout_jours * 24
        //    heures.val(h);
        //}
        new_days_sec = (jours.val() * 1 + h / 24 ) * secjour;
        logToHours(new_days_sec);
        valEnd = date2String(new Date(new Date(start.val()).getTime() + new_days_sec));
        end.val(valEnd);

        console.info(h);
        makeRoute();
    };
    /**
     * la fin a changé, changer le nombre de jours selon la nouvelle durée
     */
    updateFromEnd = function () {
        console.log('updateFromEnd');
        getVals();
        // différence entre début et nouvelle fin
        var newdiff = diff;
        console.warn(newdiff); // diff en heures
        new_days_sec = Math.round((jours.val() * 1 + heures.val() / 24 ) * msec);
        valStart = date2String(new Date(new Date(end.val()).getTime() - new_days_sec));
        start.val(valStart);
        updateProgression()
    }
    updatefromPagesFinies = function(){
        updateProgression();
        updateEnd();
    }
    set23 = function(){
        getVals();
        start.val('2015-03-28 12:0:0')
        updateStart();
    }
    $('#24hbd #pages , #24hbd #pages_finies').on('change, keyup', updateAll);
    $('#24hbd #start').on('change, keyup', updateStart);
    $('#24hbd #end ').on('change, keyup', updateFromEnd);
    $('#24hbd #jours ').on('change, keyup', updateJours);
    $('#24hbd #heures ').on('change, keyup', updateEnd);
    $('#24hbd #pages_finies ').on('change, keyup', updatefromPagesFinies);
    $('#24hbd #launch ').on('click', save);
    $('#24hbd #reset ').on('click', reset);
    init();
    set23();
    //updateAll();


    /*
     * mettre à jour la progression toutes les x secondes
     */
    setInterval(updateProgression(), 2 * 1000);

});
