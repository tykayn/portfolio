<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    #[Route('/', name: "accueil")]
    public function index()
    {
        return $this->render('Default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'page' => 'accueil'
        ]);
    }

    /**
     * @Route("/page/{page}", name="view_page")
     */
    public function viewPage($page)
    {
        return $this->render('Pages/' . $page . '.html.twig', [
            'controller_name' => 'DefaultController',
	        'page' => $page
        ]);
    }
 /**
     * @Route("/contact" )
     */
    public function viewPageContact()
    {
        return $this->render('Pages/contact.html.twig', [
            'controller_name' => 'DefaultController',
	        'page' => 'contact'
        ]);
    }

    /**
     * @Route("/page-privacy/{page}", name="view_page_privacy")
     */
    public function viewPagePrivacy($page)
    {
        return $this->render('Pages/privacy/' . $page . '.html.twig', [
            'controller_name' => 'DefaultController',
            'page' => $page
        ]);
    }
}
